docker run -d --name mysqldb-testdata -d  --env MYSQL_DATABASE=demo --env MYSQL_USER=mysql --env MYSQL_PASSWORD=mysql --env MYSQL_ROOT_PASSWORD=supersecret jr00n/blog-jee-db-testdata
docker run -d --name blog-jee-test -p 8081:8080 -p 9991:9990 --link mysqldb-testdata:database --env MYSQL_URI=database:3306 jr00n/blog-jee
