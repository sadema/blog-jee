docker run -d --name mysqldb --env MYSQL_DATABASE=demo --env MYSQL_USER=mysql --env MYSQL_PASSWORD=mysql --env MYSQL_ROOT_PASSWORD=supersecret jr00n/blog-jee-db-geendata
docker run -d --name blog-jee -p 8080:8080 -p 9990:9990 --link mysqldb:database --env MYSQL_URI=database:3306  jr00n/blog-jee
